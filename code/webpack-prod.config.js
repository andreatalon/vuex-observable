const path = require('path')
const pkg = require('./package.json')
const webpack = require('webpack')
const CompressionPlugin = require('compression-webpack-plugin')
const CopyWebpackPlugin = require('copy-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const {VueLoaderPlugin} = require('vue-loader')
const HappyPack = require('happypack');
const HardSourceWebpackPlugin = require('hard-source-webpack-plugin')
// const WebpackCleanPlugin = require('webpack-clean')

const happyThreadPool = HappyPack.ThreadPool({size: 4});

module.exports = {
  mode: 'production',
  entry: {
    vendor: Object.keys(pkg.dependencies),
    index: path.resolve(__dirname, 'src/index.js')
  },
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'js/[name].js'
  },
  optimization: {
    splitChunks: {
      cacheGroups: {
        vendor: {
          chunks: 'initial',
          name: 'vendor',
          test: 'vendor',
          enforce: true
        },
      }
    },
    runtimeChunk: false
  },
  resolve: {
    alias: {
      '../../theme.config$': path.resolve(__dirname, 'semantic_theme/theme.config')
    },
    modules: [
      path.resolve(__dirname, 'node_modules'),
      path.resolve(__dirname, 'src')
    ],
    extensions: ['.js', '.vue']
  },
  node: {
    Buffer: false,
    fs: 'empty'
  },
  externals: {
    winston: 'winston'
  },
  plugins: [
    new CopyWebpackPlugin([
      path.resolve('src/sw.js'),
      path.resolve('src/manifest.json'),
      {from: path.resolve(__dirname, 'node_modules/semantic-ui-less/themes/default/assets'), to: 'assets'},
      {from: path.resolve(__dirname, 'src/assets/favicons'), to: 'assets/favicons'},
      {from: path.resolve(__dirname, 'src/assets/fonts'), to: 'assets/fonts'},
      {from: path.resolve(__dirname, 'src/assets/images'), to: 'assets/images'}
    ]),
    new MiniCssExtractPlugin({filename: 'css/index.css'}),
    new webpack.optimize.OccurrenceOrderPlugin(),
    new CompressionPlugin({
      test: /(js|css)\/[^\n]+\.(js|css)?$/
    }),
    new HtmlWebpackPlugin({
      filename: 'index.html',
      indexCss: '/css/index.css',
      template: path.resolve(__dirname, 'src/index.ejs'),
      inject: false
    }),
    new VueLoaderPlugin(),
    new HardSourceWebpackPlugin(),
    // new HappyPack({ // vue-loader not compatible
    //   id: 'vue',
    //   loaders: ['vue-loader'],
    //   threadPool: happyThreadPool, debug: true, verbose: true
    // }),
    new HappyPack({
      id: 'js',
      loaders: ['babel-loader', {loader: 'eslint-loader', options: {include: path.resolve(__dirname, 'src'), enforce: 'pre', failOnError: true}}],
      threadPool: happyThreadPool, debug: true, verbose: true
    }),
    new HappyPack({
      id: 'css',
      loaders: ['vue-style-loader', 'style-loader', 'css-loader'],
      threadPool: happyThreadPool, debug: true, verbose: true
    })
    // new HappyPack({ // MiniCssExtractPlugin.loader not compatible
    //   id: 'less',
    //   loaders: ['vue-style-loader', 'css-loader', 'less-loader'], // less-loader?sourceMap,
    //   threadPool: happyThreadPool, debug: true, verbose: true
    // }),
    // new WebpackCleanPlugin([
    //   'dist/css/index.css',
    //   'dist/js/vendor.js',
    //   'dist/js/index.js'
    // ])
  ],
  module: {
    noParse: /lodash\/lodash.js/,
    rules: [
      {test: /\.vue$/, exclude: /node_modules/, use: 'vue-loader'}, // 'happypack/loader?id=vue'
      {test: /\.js$/, exclude: /node_modules/, use: 'happypack/loader?id=js'},
      {test: /\.html$/, use: [{loader: 'file-loader?name=[name].[ext]'}]},
      {test: /\.css$/, exclude: /node_modules/, use: 'happypack/loader?id=css'},
      {test: /\.less$/, exclude: /node_modules/, use: [MiniCssExtractPlugin.loader, 'css-loader', 'less-loader']},
      {test: /\.otf?$/, use: [{loader: 'url-loader?limit=10000&mimetype=application/octet-stream'}]},
      {test: /\.(ttf|otf|eot|svg|woff(2)?)(\?[a-z0-9]+)?$/, use: [{loader: 'file-loader', options: {name: 'assets/fonts/[name].[ext]'}}]},
      {test: /\.(png|jpg|gif)$/, loader: 'file-loader'}
    ]
  }
}
