import Vue from 'vue'
import VueRouter from 'vue-router'
import VueRx from 'vue-rx'
import SuiVue from 'semantic-ui-vue'
import {Observable} from 'rxjs'
import App from './app/App.vue'
import router from './app/misc/router'
import {store} from './app/misc/store'
import './index.less'

Vue.config.productionTip = false
Vue.use(VueRouter)
Vue.use(VueRx, {
  Observable
})
Vue.use(SuiVue)

new Vue({
  el: '#root',
  router,
  store,
  render: h => h(App)
  // template: '<App/>'
  // components: {App}
})

// "scripts": {
//   "serve": "vue-cli-service serve",
//   "build": "vue-cli-service build",
//   "lint": "vue-cli-service lint"
// },
