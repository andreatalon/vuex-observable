import {types} from '../actions/actions'

const initialState = {
  warehouse: []
}

function warehouse(state = initialState, action) {
  switch (action.type) {
  case types.STORE_WAREHOUSE:
    return Object.assign({}, state, {
      warehouse: action.warehouse
    })

  default:
    return Object.assign(initialState, state)
  }
}

export default warehouse
