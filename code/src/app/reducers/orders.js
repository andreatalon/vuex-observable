import {types} from '../actions/actions'

const initialState = {
  orders: []
}

function orders(state = initialState, action) {
  switch (action.type) {
  case types.STORE_ORDERS:
    return Object.assign({}, state, {
      orders: action.orders
    })
  
  default:
    return Object.assign(initialState, state)
  }
}

export default orders
