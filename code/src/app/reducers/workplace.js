import {types} from '../actions/actions'
import _ from 'lodash'
import misc from '../misc/misc'

const initialState = {
  activeCalls: [],
  blockingCalls: [],
  silentCalls: [],
  accessToken: localStorage.getItem('accessToken') || '',
  usersStored: false,
  users: [],
  env: {},
  flashedMessage: null,
  callResponse: null
}

function workplace(state = initialState, action) {
  switch (action.type) {
  case types.WAITING: {
    const sacs = state.activeCalls
    const sbcs = state.blockingCalls
    const sscs = state.silentCalls

    const aacs = misc.empty(action.activeCalls) ? [] : action.activeCalls
    const abcs = misc.empty(action.blockingCalls) ? [] : action.blockingCalls
    const ascs = misc.empty(action.silentCalls) ? [] : action.silentCalls

    const activeCalls = action.toggle ? _.uniq(_.concat(sacs, aacs)) : _.difference(sacs, aacs)
    const blockingCalls = action.toggle ? _.uniq(_.concat(sbcs, abcs)) : _.difference(sbcs, abcs)
    const silentCalls = action.toggle ? _.uniq(_.concat(sscs, ascs)) : _.difference(sscs, ascs)

    return Object.assign({}, state, {
      activeCalls,
      blockingCalls,
      silentCalls
    })
  }

  case types.UPDATE_ACCESS_TOKEN:
    return Object.assign({}, state, {
      accessToken: action.accessToken
    })

  case types.FLASH_MESSAGE:
    return Object.assign({}, state, {
      flashedMessage: action.flashedMessage,
      messageType: action.messageType
    })

  case types.MANAGE_RESPONSE:
    return Object.assign({}, state, {
      callResponse: action.callResponse
    })

  case types.STORE_USERS:
    return Object.assign({}, state, {
      usersStored: true,
      users: action.users
    })

  case types.STORE_ENV:
    return Object.assign({}, state, {
      env: misc.empty(action.env) ? state.env : Object.assign({}, state.env, action.env)
    })

  default:
    return Object.assign(initialState, state)
  }
}

export default workplace
