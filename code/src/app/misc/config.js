import misc from './misc'
// import api from './api'

const config = {
  actionTimeout: 3000,

  auth0: {
    DOMAIN: '',
    CLIENT_ID: '',
    CALLBACK_URL: `${window.location.origin}/callback`,
    LOGOUT_URL: `${window.location.origin}/logout`
  },

  PROD: !window.location.host.match(/(localhost|stg\-)/),
  // TEST: false,

  protocol() {
    return 'https:' // window.location.protocol
  },

  host() {
    if (window.location.host.match(/^localhost/)) {
      return 'i2zv4bpvx5.execute-api.eu-west-1.amazonaws.com/dev'
    }

    if (window.location.host.match(/^stg-/)) {
      return 'wi25gux7qk.execute-api.eu-west-1.amazonaws.com/staging'
    }

    return 'w2zl3ozcnf.execute-api.eu-west-1.amazonaws.com/production'
  },

  ports() {
    switch (window.location.port) {
    case '41':
    case '4141':
    case '':
      return {be: '', ws: ''}
    default:
      return {be: ':' + window.location.port, ws: ':' + window.location.port}
    }
  },

  baseUrl() {
    return this.protocol() + '//' + this.host() + this.ports().be // + '/' + api.v
  },

  uri(uri) {
    return this.baseUrl() + uri
  },

  wsUrl() {
    return 'wss:' // (this.protocol() === 'http:' ? 'ws:' : 'wss:') + '//' + this.host() + this.ports().ws
  },

  api(uri) {
    return this.baseUrl() + uri
  },

  headers() {
    return {
      'Content-Type': 'application/json'
    }
  },

  authorizedHeaders() {
    return Object.assign(this.headers(), {
      'Authorization': 'Bearer ' + localStorage.getItem('idToken')
    })
  },

  hasAuthorizedHeaders() {
    return !misc.empty(localStorage.getItem('idToken'))
  }
}

export default config
