import {createEpicMiddleware, combineEpics} from 'redux-observable'
import misc from './misc'
import * as epics from '../actions/epics'

const observables = () => {
  const epicMiddleware = createEpicMiddleware()
  const rootEpic = combineEpics(...Object.values(epics))

  // Creates the main Vuex plugin function.
  return store => {
    // Defines a store model that conforms to the redux-observable middleware api.
    const storeModel = {
      // Returns a new object reference if state has changed in order to
      // satisfy the state immutability assumption of the redux-observable api.
      getState() {
        if (!misc.objectLevelCompare(this.stateCopy, {state: store.state, getters: store.getters})) {
          this.stateCopy = Object.assign({}, {state: store.state, getters: store.getters})
        }

        return this.stateCopy
      },
      dispatch: function() {store.commit('commit', arguments[0])},
      stateCopy: Object.assign({}, {state: store.state, getters: store.getters})
    }

    // Instantiates the redux-observable middleware and returns a function that can trigger the middleware.
    store._dispatchEpic = epicMiddleware(storeModel)(action => action)
    // Sets up a listener for mutations in order to update the store stream even when an epic hasn't been dispatched.
    store.subscribe(mutation => store._dispatchEpic(mutation.payload))
    // Runs the middleware with the root epic.
    epicMiddleware.run(rootEpic)
    // Intercepts the actions in order to initialize undefined actions as epic dispatchers.
    // This is done as to prevent the 'unknown-action-type' error when dispatching inside of an action.
    // store._actions = new Proxy(store._actions, {
    //   get(actions, type) {
    //     return [payload => Promise.resolve(store._dispatchEpic({type, ...payload})]
    //   }
    // })
  }
}

export default observables