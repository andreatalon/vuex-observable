
import VueRouter from 'vue-router'
import Warehouse from '../components/Warehouse.vue'
import Orders from '../components/Orders.vue'
import Login from '../components/Login.vue'
import Callback from '../components/Callback.vue'
import Logout from '../components/Logout.vue'
import NoMatch from '../components/NoMatch.vue'

const router = new VueRouter({
  mode: 'history',
  routes: [
    {path: '/', redirect: '/warehouse'},
    {path: '/warehouse', component: Warehouse}, // , alias: '/'
    {path: '/orders', component: Orders},
    {path: '/login', component: Login},
    {path: '/callback', component: Callback},
    {path: '/logout', component: Logout},
    {path: '*', component: NoMatch}
  ]
})

export default router