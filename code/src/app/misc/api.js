const api = {
  v: '1',

  // GET_ENV: '/env',
  GET_WAREHOUSE: '/v1/warehouse',
  ROUTE_ORDERS: '/v1/order'

  // POST_LOGIN: 'https://<oauth_domain>/v2/token' // '/auth/login',
  // POST_REFRESH: '/auth/refresh',
  // POST_LOGOUT: '/auth/logout',
}

export default api
