// import * as auth0 from 'auth0-js'
import auth0 from 'auth0-js'
import config from './config'

const auth = new auth0.WebAuth({
  domain: config.auth0.DOMAIN,
  clientID: config.auth0.CLIENT_ID,
  redirectUri: config.auth0.CALLBACK_URL,
  audience: `https://${config.auth0.DOMAIN}/userinfo`,
  returnTo: config.auth0.LOGOUT_URL,
  responseType: 'token id_token',
  scope: 'openid',
  leeway: 60
})

export default auth
