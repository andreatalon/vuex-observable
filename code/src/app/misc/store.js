import Vue from 'vue'
import Vuex from 'vuex'
import observables from './observable' // 'vuex-observable-plugin'
import orders from '../reducers/orders'
import warehouse from '../reducers/warehouse'
import workplace from '../reducers/workplace'
import {defs} from '../actions/actions'
import * as epics from '../actions/epics'
import misc from './misc'

Vue.use(Vuex)

const actions = {}
const starting_epic_actions = Object.keys(epics).map(e => misc.camelToSnake(e.replace('Epic', '')))

Object.keys(defs).map(def => {
  const type = misc.camelToSnake(def)
  
  actions[type] = ({commit}, data) => commit('commit', {type, ...data})
})

const commit = {'commit': function() {
  const args = Array.from(arguments)

  args.shift()

  const params = Object.assign({}, ...args)
  const type = params.type

  if (starting_epic_actions.indexOf(type) < 0) {
    store.commit('commit', params)

    return
  }

  delete params.type
  store.dispatch(type, params)
}}

const combine = (state, action = {type: 'UNDEFINED'}) => {
  const reducers = [orders, warehouse, workplace] // , other reducers
  let newState = state
  
  reducers.forEach(reducer => {
    newState = reducer(newState, action)
  })

  return newState
}

const store = new Vuex.Store({
  state: combine(),
  mutations: {
    commit(state, action) {
      const newState = combine(state, action)

      for (const prop in newState) {
        state[prop] = newState[prop]
      }
    }
  },
  actions,
  plugins: [observables()]
})

export {commit, store}