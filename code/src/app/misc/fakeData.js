const fakeData = {
  warehouse: [
    {'warehouseElementId': 1, 'name': 'A01 500 gr', 'quantity': 250, 'threshold': 100},
    {'warehouseElementId': 2, 'name': 'A02 650 gr', 'quantity': 150, 'threshold': 100},
    {'warehouseElementId': 3, 'name': 'A03 450 gr', 'quantity': 100, 'threshold': 100},
    {'warehouseElementId': 4, 'name': 'A04 600 gr', 'quantity': 50, 'threshold': 100},
    {'warehouseElementId': 5, 'name': 'A05 398 gr', 'quantity': 200, 'threshold': 100},
    {'warehouseElementId': 6, 'name': 'A06 260 gr', 'quantity': 170, 'threshold': 100},
    {'warehouseElementId': 7, 'name': 'B01 235 gr', 'quantity': 200, 'threshold': 100},
    {'warehouseElementId': 8, 'name': 'B02 215 gr', 'quantity': 200, 'threshold': 100},
    {'warehouseElementId': 9, 'name': 'B03 115 gr', 'quantity': 210, 'threshold': 100},
    {'warehouseElementId': 10, 'name': 'B04 130 gr', 'quantity': 200, 'threshold': 100},
    {'warehouseElementId': 11, 'name': 'B05 150 gr', 'quantity': 50, 'threshold': 100},
    {'warehouseElementId': 12, 'name': 'B06 240 gr', 'quantity': 160, 'threshold': 100},
    {'warehouseElementId': 13, 'name': 'B07 95 gr', 'quantity': 80, 'threshold': 100}
  ],
  orders: [
    {'ordersElementId': 1, 'name': 'B07 95 gr', 'quantity': 100, 'date': '2019-06-30', 'status': 'evaso'},
    {'ordersElementId': 2, 'name': 'A04 600 gr', 'quantity': 150, 'date': '2019-06-30', 'status': 'pronto'},
    {'ordersElementId': 3, 'name': 'B05 150 gr', 'quantity': 150, 'date': '2019-07-01', 'status': 'pronto'}
  ]
}

export default fakeData
