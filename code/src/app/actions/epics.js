import {concat, empty, of} from 'rxjs'
import {ajax} from 'rxjs/ajax'
import {catchError, delay, filter, mergeMap, switchMap} from 'rxjs/operators'
import {actions, types} from '../actions/actions'
import config from '../misc/config'
import api from '../misc/api'
// import translate from '../misc/translate'
import misc from '../misc/misc'
import fakeData from '../misc/fakeData'

export const flashMessageAndResetEpic = action$ =>
  action$.pipe(filter(action => action.type === types.FLASH_MESSAGE_AND_RESET), switchMap(action =>
    concat(
      of(actions.flashMessage(null)),
      of(actions.flashMessage(action.flashedMessage, action.messageType)),
      of(actions.flashMessage(null)).pipe(delay(6000))
    )))

export const manageResponseAndResetEpic = action$ =>
  action$.pipe(filter(action => action.type === types.MANAGE_RESPONSE_AND_RESET), switchMap(action =>
    concat(
      of(actions.manageResponse(action.callResponse)),
      of(actions.manageResponse(null))
    )))

export const callEnvEpic = action$ =>
  action$.pipe(filter(action => action.type === types.CALL_ENV), switchMap(() =>
    concat(
      of(actions.waiting(true, ['callEnvEpic'])),
      ajax({
        url: api.GET_ENV,
        method: 'GET',
        headers: config.headers()
      })
        .pipe(
          mergeMap(response => of(actions.storeEnv(response.response.data))),
          catchError(error => of(actions.manageResponseAndReset(error)))
        ),
      of(actions.waiting(false, ['callEnvEpic']))
    )))

export const callWarehouseEpic = action$ =>
  action$.pipe(filter(action => action.type === types.CALL_WAREHOUSE), switchMap(() =>
    concat(
      of(actions.waiting(true, ['callWarehouseEpic'])),
      of(actions.storeWarehouse(fakeData.warehouse)),
      // ajax({
      //   url: config.api(api.GET_WAREHOUSE),
      //   method: 'GET',
      //   headers: config.authorizedHeaders()
      // })
      //   .pipe(
      //     mergeMap(response => of(actions.storeWarehouse(response.response))),
      //     catchError(error => of(actions.manageResponseAndReset(error)))
      //   ),
      of(actions.waiting(false, ['callWarehouseEpic']))
    )))

export const callOrdersEpic = action$ =>
  action$.pipe(filter(action => action.type === types.CALL_ORDERS), switchMap(() =>
    concat(
      of(actions.waiting(true, ['callOrdersEpic'])),
      of(actions.storeOrders(fakeData.orders)),
      // ajax({
      //   url: config.api(api.ROUTE_ORDERS),
      //   method: 'GET',
      //   headers: config.authorizedHeaders()
      // })
      //   .pipe(
      //     mergeMap(response => of(actions.storeOrders(response.response))),
      //     catchError(error => of(actions.manageResponseAndReset(error)))
      //   ),
      of(actions.waiting(false, ['callOrdersEpic']))
    )))

export const callInitEpic = action$ =>
  action$.pipe(filter(action => action.type === types.CALL_INIT), switchMap(() =>
    concat(
      // of(actions.callEnv()),
      // of(actions.callWarehouse()),
      // of(actions.callOrders())
    )))

export const modWarehouseElementEpic = action$ =>
  action$.pipe(filter(action => action.type === types.MOD_WAREHOUSE_ELEMENT), switchMap(action =>
    concat(
      of(actions.waiting(true, ['modWarehouseElementEpic'])),
      ajax({
        url: config.api(`${api.GET_WAREHOUSE}/${action.warehouseElement.warehouseElementId}`),
        method: 'PUT',
        headers: config.authorizedHeaders(),
        body: action.warehouseElement
      })
        .pipe(
          mergeMap(response => of(actions.manageResponseAndReset({code: 200, message: response.response.message}))),
          catchError(error => of(actions.manageResponseAndReset(error)))
        ),
      of(actions.waiting(false, ['modWarehouseElementEpic']))
    )))

export const createNewOrderEpic = action$ =>
  action$.pipe(filter(action => action.type === types.CREATE_NEW_ORDER), switchMap(action =>
    concat(
      of(actions.waiting(true, ['createNewOrderEpic'])),
      ajax({
        url: config.api(api.ROUTE_ORDERS),
        method: 'POST',
        headers: config.authorizedHeaders(),
        body: action.orderItems
      })
        .pipe(
          mergeMap(response => of(actions.callOrders())),
          catchError(error => of(actions.flashMessageAndReset(error.response.message, misc.messageType.ERROR)))
        ),
      of(actions.waiting(false, ['createNewOrderEpic']))
    )))
