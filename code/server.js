// node http-server dist -g -p 80

const connect = require('connect')
// const log4js = require('log4js')
// const morgan = require('morgan')
// const compression = require('compression')
// const bodyParser = require('body-parser')
const serveStatic = require('serve-static')
const historyApiFallback = require('connect-history-api-fallback')
const gzipStatic = require('connect-gzip-static')
// const shelljs = require('shelljs')

// shelljs.exec('APP_NAME=abbasanta-fe /etc/env-from-ps.sh')

const app = connect()
// app.use(morgan('default', {
//   stream: {
//     write: log => {
//       (log4js.getLogger()).debug(log)
//     }
//   }
// }))
app.use(function(req, res, next) {
  if (req._parsedUrl.pathname.match(/healthcheck/)) {
    res.writeHead(200)
    res.end('I\'m fine!')
    return
  }
  if (req._parsedUrl.pathname.match(/env/)) {
    res.writeHead(200)
    res.end(JSON.stringify({'data': Object.keys(process.env).reduce((prev, next) => {
      if (next.match(/^(?=^[A-Z]+(?:_[A-Z]+)*$)(?=^((?!(AWS|CWD|ECS|GIT|HOME|HOST|NODE|NPM|PATH|PWD|SASS|SHLVL|SKIP|YARN)).)*$).*$/)) {
        prev[next] = process.env[next]
      }

      return prev
    }, {})}))
    return
  }
  next()
})
// app.use(compression())
// app.use(bodyParser.urlencoded({extended: false}));
app.use(serveStatic(__dirname + '/dist'))
app.use(historyApiFallback())
app.use(gzipStatic(__dirname + '/dist', {maxAge: 86400000, override: true}))
app.use(function(req, res, next) {
  if (req._parsedUrl.pathname.match(/(index|theme|vendor)\.(j|cs)s$/)) {
    req.url += '.gz'
    res.setHeader('Content-Encoding', 'gzip')
  } else {
    req.url = '/'
  }
  next()
})

app.listen(80)
